package conntlo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class Userupdate
 */
@WebServlet("/Userupdate")
public class Userupdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Userupdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		if (user == null) {

			response.sendRedirect("LoginServret");

			return;

		}

		String id = request.getParameter("id");

		UserDao userdao = new UserDao();
		User rewrite = userdao.Rewrite(id);

		request.setAttribute("userupdate", rewrite);

		//String Temporary = ("userupdate",password);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_kousin.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String birthDate = request.getParameter("birthDate");

		// パスワードとパスワード確認用が違っている

		if (!password.equals(password1)) {

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_kousin.jsp");
			dispatcher.forward(request, response);

			return;
		}
		// 入力項目が一つでもかけていた場合
		if (id.equals("") || name.equals("") || birthDate.equals("")) {

			request.setAttribute("errMsg", "項目に未入力の項目があります");

			// 再フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_kousin.jsp");
			dispatcher.forward(request, response);

			return;
		}

		if (password.equals("")&&password1.equals("") ) {

			UserDao userDao = new UserDao();
			userDao.Rewrite3(name, birthDate, id);

			response.sendRedirect("UserListServlet");

			return;
		}

		UserDao userDao = new UserDao();
		userDao.Rewrite2(name, password, birthDate, id);

		response.sendRedirect("UserListServlet");

	}

}
