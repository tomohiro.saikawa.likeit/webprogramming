package conntlo;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/Userdetail")
public class Userdetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Userdetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る		HttpSession session = request.getSession();
		
		
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {


			response.sendRedirect("LoginServret");



			return;


		}
		


	


	

		String id = request.getParameter("id");

		UserDao userdao = new UserDao();
		User serachuser = userdao.Browsing(id);



		request.setAttribute("userdetail", serachuser);






		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_details.jsp");
		dispatcher.forward(request, response);







		// 確認用：idをコンソールに出力
		System.out.println(id);



		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する

		// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード

	}


}
