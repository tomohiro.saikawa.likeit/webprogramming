package conntlo;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDao;
import model.User;

/**
 * Servlet implementation class Usersignup
 */
@WebServlet("/Usersignup")
public class Usersignup extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Usersignup() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		if(user == null) {


			response.sendRedirect("LoginServret");



			return;


		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_apo.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");


		// パスワードとパスワード確認用が違っている
		if(!password.equals(password2)) {

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_apo.jsp");
			dispatcher.forward(request, response);


			return;
		}



		// ログインIDが被っている
		UserDao userDao = new UserDao();
		User user = userDao.Idcover(loginId);
		if(user != null ) {

			request.setAttribute("errMsg", "ログインIDの登録にエラーが発生しました");

			// 再フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_apo.jsp");
			dispatcher.forward(request, response);


			return;

		}




		// 入力項目が一つでもかけていた場合
		if(loginId.equals("") || name.equals("") || birthDate.equals("")) {



			request.setAttribute("errMsg", "項目に未入力の項目があります");

			// 再フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_apo.jsp");
			dispatcher.forward(request, response);


			return;
		}








		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userdao = new UserDao();
		userdao.usersignup(loginId, password, name, birthDate);

		response.sendRedirect("UserListServlet");

	}

}
