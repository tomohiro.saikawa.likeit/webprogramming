<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">





</head>
<body>

	<nav>

		<div class="row col-2 loginInfo">
			<li class="navbar-text">${userInfo.name} さん </li>
			<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>

	</nav>
	<h1 style="text-align: center;">ユーザ一覧</h1>

	<div aligin="right">
		<a href="Usersignup" class="navbar-link logout-link">新規登録</a>
	</div>


	<form method="post" action="UserListServlet">
		<div class="col-sm-12">
			<div align="center">
				<p>
					ログインID ：<input type="text" name="loginId" size="30" >
				</p>
				<p>
					ユーザー名 ：<input type="text" name="name" size="30" >
				</p>
				<tr>
					<td align="right"><b> 生年月日：</b></td>
					<td><input type="date" name="startdate"></td>
					<b> ~ </b>
					<input type="date" name="enddate">
				</tr>








				<input type="submit" value="検索" class="btn btn-primary">

				<div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach  items="${userList}" var="user"  >
                   <tr>


                   <td>${user.loginId}</td>




                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="Userdetail?id=${user.id}">詳細</a>
                       <c:if test="${userInfo.id == 1 || userInfo.id ==user.id }">
                       <a class="btn btn-success" href="Userupdate?id=${user.id}">更新</a>
                       </c:if>
                       <c:if test="${userInfo.id == 1 }">
                       <a class="btn btn-danger" href ="Userdelete?id=${user.id}">削除</a>
                       </c:if>
                     </td>
                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>




			</div>
		</div>



	</form>
</body>
</html>