﻿CREATE DATABASE usermanagment DEFAULT CHARACTER SET utf8;
USE usermanagment;

CREATE TABLE user(
id SERIAL PRIMARY KEY AUTO_INCREMENT,
login_id varchar(255) UNIQUE Not Null,
name varchar(255) Not NUll,
birth_date DATE Not NUll,
password varchar(255) Not NUll,
create_date DATETIME Not Null,
update_date DATETIME Not Null);

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES('admin','管理者','2020-01-01','yuugou',now(),now());

